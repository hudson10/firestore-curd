import React, {useEffect, useState} from 'react'
import firebase from "./firebase"
import Update from "./component/update";

function FirestoreService() {

    const [records, setRecords] = useState([{ }])                //db documents
    
    const [updateToggle, setUpdateToggle] = useState(false)      //updateToggle
    const [userID, setuserID] = useState(" ");                   //selected user ID
    const [userUpdate, setUserUpdate] = useState(" ");           //Document of selected user ID  
    
    
    const dbRef = firebase.collection("/post"); 
    const getRecords =() => 
    {
        dbRef.get().then(snapshot => {
            const post =[]
            snapshot.forEach(childSnapshot => 
                {
                var recordId = childSnapshot.id;
                var data = childSnapshot.data();
                post.push({
                    id: recordId,
                    Name: data.Name,
                    Email: data.Email,
                    Mobile: data.Mobile,
                    Address: data.Address
                })
            })
                setRecords(post)
       })
    }

    useEffect(() => {
        getRecords()
    return () => {
        }
    }, []) 

//on click edit button
   const onEdit = (e) =>                           
     {
        const newArray = records.filter(item => item.id === e)
        setUserUpdate(newArray);
        setuserID(e);
        setUpdateToggle(true);
     }
 
//on click update button 
    const onUpdate = (e) => 
    {
        getRecords()
        setUpdateToggle(false);  
    }
    
//on click delete button
    const ondelete = (e) => 
    {
        const newArray = records.filter(item => item.id !== e)
        dbRef.doc(e).delete();
        setRecords(newArray);
        setUpdateToggle(false);  
    }

  
    return (
        <div>
             <div className="col-md-auto" style={{overflowX:"auto"}}>
                <table className="table table-hover table-striped" >
                   <thead className="thead-dark">
                       <tr>
                       <th>Name</th>
                       <th>Mobile</th>
                       <th className="w-25">Email</th>
                       <th>Address</th>
                       <th >Edit/Remove</th>
                       </tr>
                   </thead>
                   <tbody>
                       {records.length === 0  ? <tr><td>Loading....</td></tr>: 
                        (
                            Object.keys(records).map((e,i) => {

                          return <tr key={i}>
                                 <td>{records[e].Name}</td>
                                 <td>{records[e].Mobile}</td>
                                 <td>{records[e].Email}</td>
                                 <td>{records[e].Address}</td>
                                 <td>
                                 <p className="btn text-primary" onClick={()=> onEdit(records[e].id)}>
                                         <i className="fas fa-user-edit"></i>
                                     </p>
                                     <p className="btn text-danger" onClick={()=> ondelete(records[e].id)}>
                                     <i className="fas fa-user-times" ></i>
                                     </p>
                                 </td>
                                </tr>
                              }
                           )          
                        )
                   }   
                   </tbody>
                </table>
            </div>  
           <div style={ {display: updateToggle ? null : "none"} }>
           <Update formPreviousData={userUpdate} userID={userID} formRecords={onUpdate} />
           </div>
        </div>
    )
}

export default FirestoreService
