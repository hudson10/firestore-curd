import React from 'react';
import './App.css';
import Form from "./component/form";
import Table from "./component/table";
import 'react-toastify/dist/ReactToastify.css';

import { Switch, Route, Link } from "react-router-dom";

function App() {
  return (
    <div>
    <nav className="navbar navbar-expand navbar-dark bg-dark">
       <p className="navbar-brand">
        Firestore CRUD Operations 
       </p>

      <div className="navbar-nav mr-auto">
        <li className="nav-item">
          <Link to={"/"} className="nav-link">
            Create
          </Link>
        </li>
        <li className="nav-item">
          <Link to={"/records"} className="nav-link">
            Data
          </Link>
        </li>
      </div>
    </nav>

    <div className="container mt-3">
      <h2>React Firestore CRUD</h2>
      <Switch>
        <Route exact path="/" component={Form} />
        <Route exact path="/records" component={Table} />
      </Switch>
    </div>
  </div>
  );
}

export default App;
