import React, {useState, useEffect} from 'react'
import firebase from "../firebase"


function Update({formPreviousData , userID, formRecords }) {
    
    const dbRef = firebase.collection("/post");

    const [formData, setformData] = useState({
            Name:" ",
            Email:" ",
            Mobile:" ",
            Address:" "
    })

    
    const onChangeValue = (e) => {
        var {name ,value} = e.target;

        setformData({
            ...formData,
            [name]: value
        })
    }


    useEffect(() => {
        
            setformData({
                Name:formPreviousData[0].Name,
                Email:formPreviousData[0].Email,
                Mobile:formPreviousData[0].Mobile,
                Address:formPreviousData[0].Address
                })
    }, [formPreviousData, formRecords])

    const onHandleUpdate = (e) =>
    {
        e.preventDefault();
        dbRef.doc(userID).update(formData)
       .then((docRef)=> {
        formRecords()
       })
        .catch(function(error) {
            alert("Error update information: ", error)
        });
    }
    return (
        <div className="center">
             <form autoComplete="off" onSubmit={onHandleUpdate}>
            <div className="form-group input-group">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="far fa-user"></i>
                    </div>
               </div>
               <input className="form-control" placeholder="Full Name" name="Name" onChange={onChangeValue} value={formData.Name}/>
             </div>

                <div className="form-group input-group ">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                        <i className="far fa-envelope"></i>
                        </div>
                </div>
                <input className="form-control" placeholder="Email" name="Email" onChange={onChangeValue} value={formData.Email}/>
                </div>

                <div className="form-group input-group">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                        <i className="fas fa-mobile-alt"></i>
                        </div>
                </div>
                <input className="form-control" placeholder="Mobile" name="Mobile" onChange={onChangeValue} value={formData.Mobile}/>
                </div>

                 <div className="form-group">
                  <textarea className="form-control" placeholder="Address" name="Address" onChange={onChangeValue} value={formData.Address}/>
                </div>
                
                <div className="form-group">
                  <input type="submit" className="btn btn-primary btn-block" value="update"/>
                </div>
        </form>
        </div>
    )
}

export default Update;
