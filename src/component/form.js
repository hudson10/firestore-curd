import React, {useState} from 'react'
import firebase from "../firebase"

import { ToastContainer, toast } from 'react-toastify';

function Form() {
    const [formData, setformData] = useState({})

    const onChangeValue = (e) => {
        var {name ,value} = e.target;

        setformData({
            ...formData,
            [name]: value
        })
    }

    const onHandleSubmit = (e) =>
    {
        e.preventDefault();
        const dbRef = firebase.collection("/post");   
        dbRef.add(formData)
        .then(function(docRef) {
            toast.success("Created")
        })
        .catch(function(error) {
            alert("Error adding Tutorial: ", error)
        });
    }
    return (
        <div className="center">
         <ToastContainer />
             <form autoComplete="off" onSubmit={onHandleSubmit}>
            <div className="form-group input-group">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="far fa-user"></i>
                    </div>
               </div>
               <input className="form-control" placeholder="Full Name" name="Name" onChange={onChangeValue}/>
             </div>

                <div className="form-group input-group ">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                        <i className="far fa-envelope"></i>
                        </div>
                </div>
                <input className="form-control" placeholder="Email" name="Email" onChange={onChangeValue}/>
                </div>

                <div className="form-group input-group">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                        <i className="fas fa-mobile-alt"></i>
                        </div>
                </div>
                <input className="form-control" placeholder="Mobile" name="Mobile" onChange={onChangeValue}/>
                </div>

                 <div className="form-group">
                  <textarea className="form-control" placeholder="Address" name="Address" onChange={onChangeValue}/>
                </div>
                
                <div className="form-group">
                  <input type="submit" className="btn btn-primary btn-block"/>
                </div>
        </form>
        </div>
    )
}

export default Form;
